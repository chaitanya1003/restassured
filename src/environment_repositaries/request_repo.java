package environment_repositaries;

import java.io.IOException;
import java.util.ArrayList;

import common_method.utilityclass;

public class request_repo extends environment {

	public static String post_param_api(String testcaseName) throws IOException {

		ArrayList<String> data = utilityclass.ReadExceldata("post_api", testcaseName);
		String req_name = data.get(1);
		String req_job = data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";
		return requestBody;
	}

	public static String post_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		return requestBody;
	}

	public static String put_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return requestBody;
	}

	public static String patch_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"chaitanya\",\r\n" + "    \"job\": \"Business Analyst\"\r\n"
				+ "}";
		return requestBody;
	}

	public static String get_request_body() {
		String requestBody = "";
		return requestBody;
	}

	public static String delete_request_body() {
		String requestBody = "";
		return requestBody;
	}

}
