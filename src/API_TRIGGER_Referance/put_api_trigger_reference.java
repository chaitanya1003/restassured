package API_TRIGGER_Referance;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class put_api_trigger_reference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Step 1 : Declare the variables
		
				String hostname = "https://reqres.in/";
				String resource = "/api/users/2";
				String headername = "Content-Type";
				String headervalue = "application/json";
				String requestBody = "{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";

				// Step 2 : Configure the API and trigger
				
				RestAssured.baseURI = hostname;
				
				String res_body = given().header(headername,headervalue).body(requestBody).when().put(resource).then().extract().response().asString();
				System.out.println(res_body);
				
				JsonPath jsp_res = new JsonPath(res_body);
				
				String res_name = jsp_res.getString("name");
				System.out.println(res_name);
				String res_job = jsp_res.getString("job");
				System.out.println(res_job);
				String res_updatedAt = jsp_res.getString("updatedAt");
				res_updatedAt = res_updatedAt.substring(0,11);
				System.out.println(res_updatedAt);
				
				JsonPath jsp_req = new JsonPath(requestBody);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");
				
				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0,11);
				
				Assert.assertEquals(res_name,req_name,"Name in res_body is not equal to the name sent in requestBody");
				Assert.assertEquals(res_job,req_job,"Job in the res_body is not equal to the njob sent in requestBody");
				Assert.assertEquals(res_updatedAt,expecteddate,"updatedAt in res_body is not equal to expecteddate in requestbody");
				
				
		 
		 

	}

}
