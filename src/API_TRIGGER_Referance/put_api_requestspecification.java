package API_TRIGGER_Referance;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class put_api_requestspecification {

    public static void main(String[] args) {
        // Base URL and resource
        String hostname = "https://reqres.in";
        String resource = "/api/users/2";
        
        // Header information
        String headername = "Content-Type";
        String headervalue = "application/json";
        
        // Request body
        String requestBody = "{\r\n"
                + "  "
        		+ "  \"name\": \"morpheus\",\r\n" 
        		+ "    \"job\": \"zion resident\"\r\n"
        		+ "}";
        
        // Step 2: Trigger the API
        // Step 2.1: Build the request specification using RequestSpecification class
        RequestSpecification req_spec = RestAssured.given();
        
        // Step 2.2: Set the header
        req_spec.header(headername, headervalue);
        
        // Step 2.3: Set the request body
        req_spec.body(requestBody);
        
        // Step 2.4: Now trigger the API
        Response response = req_spec.put(hostname + resource);
        
        // Step 3: Extract the status code
        int statusCode = response.getStatusCode();
        System.out.println("Status Code: " + statusCode);
        
        // Step 4: Fetch the response body parameters
        String responseBody = response.getBody().asString();
        System.out.println("Response Body: " + responseBody);
        
        // Parse JSON response
        JsonPath jsonPath = new JsonPath(responseBody);
        String res_name = jsonPath.getString("name");
        String res_job = jsonPath.getString("job");
        String res_updatedAt = jsonPath.getString("updatedAt");
        
        // Step 5: Fetch the request body parameters
        JsonPath jsp_req = new JsonPath(requestBody);
        String req_name = jsp_req.getString("name");
        String req_job = jsp_req.getString("job");
        
        // Step 6: Generate expected data
        LocalDateTime currentdate = LocalDateTime.now();
        String expectedDate = currentdate.toString().substring(0, 10); // Using substring to get date part only
        
        // Step 7: Validate using TestNG Assertions
        Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
        Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
        Assert.assertEquals(res_updatedAt.substring(0, 10), expectedDate, "updatedAt in ResponseBody is not equal to Date Generated");
    }
}
