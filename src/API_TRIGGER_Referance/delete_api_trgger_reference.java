package API_TRIGGER_Referance;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

//import static io.restassured.RestAssured.given;

public class delete_api_trgger_reference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Step 1: Declare the variables
				String hostname = "https://reqres.in";
				String resource = "/api/users/2";
				
				//Step 2: Build the request specification using RequestSpecification
				RequestSpecification req_spec = RestAssured.given();
				
				// Step 3: Trigger the API
				Response response = req_spec.delete(hostname  + resource);
				
				//Step 4: Extract Status code
				int statuscode = response.statusCode();
				System.out.println(statuscode);
	}

}
