package API_TRIGGER_Referance;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class post_api {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 String hostname="https://reqres.in/";
		   String resource="/api/users";
		   String requestBody="{\r\n"
		   		+ "    \"name\": \"morpheus\",\r\n"
		   		+ "    \"job\": \"leader\"\r\n"
		   		+ "}";
		   String headername="content-type";
		   String headervalue="application-json";
		   
		   RestAssured.baseURI= hostname;
		    String res_body=given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().asString();
            System.out.println(res_body);
	}

}
