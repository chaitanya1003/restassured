Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Mon, 29 Apr 2024 10:48:31 GMT
Content-Type=application/json; charset=utf-8
Content-Length=72
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714387710&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=iatRbRH74qwCOCfaRL%2FAbNB2g3%2BDI%2Fl0miPTLOTTYTs%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714387710&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=iatRbRH74qwCOCfaRL%2FAbNB2g3%2BDI%2Fl0miPTLOTTYTs%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"48-Hj8YuylIzfp2zZaXhL0xY3Xe5aA"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=87beb8582ef484aa-BOM

Response body is :
{"name":"10.2","job":"","id":"2","createdAt":"2024-04-29T10:48:30.943Z"}