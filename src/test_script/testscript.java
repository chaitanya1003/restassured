package test_script;

import java.io.File;
import java.io.IOException;

import java.time.LocalDateTime;


import org.testng.Assert;

import common_method.api_trigger;
import common_method.utilityclass;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
public class testscript extends api_trigger {
public static void execute() throws IOException {
    	
    	
	
	File logfolder = utilityclass.createFolder("Post_API");
	int statuscode = 0;
	for(int i=0; i<5; i++) {
	
	Response response = delete_api_trigger(post_request_body(), post_endpoint());

	// Step 3: Extract status code
	  statuscode = response.statusCode();
	System.out.println(statuscode);
	
	if (statuscode == 200) {

	// Step 4: Fetch response body parameters
	ResponseBody responseBody = response.getBody();
	System.out.println(responseBody.asString());
	
	utilityclass.createLogFile("Post_API_TC1", logfolder, post_endpoint(), post_request_body(), response.getHeaders().toString(), responseBody.asString());
	validate(responseBody);
	break;
	}
	
	else {
		System.out.println("Status code found in iteration: " +i+ "is: " +statuscode+ ", and is not equal to expected status code hence retrying");
	}
	}
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
}
public static void validate(ResponseBody responseBody) {
	
	String res_name = responseBody.jsonPath().getString("name");
	String res_job = responseBody.jsonPath().getString("job");
	String res_id = responseBody.jsonPath().getString("id");
	String res_createdAt = responseBody.jsonPath().getString("createdAt");
	res_createdAt = res_createdAt.toString().substring(0, 11);

	// Step 5: Fetch request body parameters
	JsonPath jsp_req = new JsonPath(post_request_body());
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");

	// Step 6: Generate expected date
	LocalDateTime currentdate = LocalDateTime.now();
	String expecteddate = currentdate.toString().substring(0, 11);

	// Step 6: Validate using TestNG assertions
	Assert.assertEquals(res_name, req_name,"Name in reposnseBody is not equal to the name sent in the requestBody");
	Assert.assertEquals(res_job, req_job, "Job in reposnseBody is not equal to the job sent in the requestBody");
	Assert.assertNotNull(res_id, "Id in reposnseBody is found to be Null");
	Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

    }

}