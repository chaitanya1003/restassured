package test_script;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import common_method.api_trigger;
import common_method.utilityclass;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class patchscript extends api_trigger {
	public static void execute() throws IOException {
		 File logfolder = utilityclass.createFolder("Patch_API");
		int statuscode=0;
		 for(int i=0; i<5;i++) {
		 
		  Response response = patch_api_trigger(patch_request_body() ,patch_endpoint());
		  //  2.4 extract the status code 
		    statuscode = response.statusCode();
		    System.out.println(statuscode);
		    if(statuscode==200) { 
		   // 3 :parse the responseBody parameter 
		    ResponseBody responseBody = response.getBody();
		     System.out.println(responseBody.asString());
		utilityclass.createLogFile("Patch_API",logfolder,patch_endpoint(),patch_request_body(),response.getHeaders().toString(),responseBody.asString());
        validate(responseBody);
        break;
 }
		    else {System.out.println(" status code found in iteration:"+i+"is:"+ statuscode+",and is not equal expected status code hence retrying");
		    }

		    Assert.assertEquals(statuscode,200,"correct sttuscode is not found even after retrying for 5 time");
		    }
		}
	
	public static void validate ( ResponseBody responseBody) {
		  String res_name = responseBody.jsonPath().getString("name");
         String res_job = responseBody.jsonPath().getString("job");  
         String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
         res_updatedAt = res_updatedAt.toString().substring(0, 11);
         
         
         // step 4 parse the RequestBody using json path extract RequestBody parameter
           
           JsonPath jsp_req = new JsonPath(patch_request_body()); 
           String req_name = jsp_req.getString("name");
           String req_job = jsp_req.getString("job");
           
           
           
           //4.1:  Generate expected Date
            LocalDateTime currentDate = LocalDateTime.now();
           String expecteddate = currentDate.toString().substring(0,11);
           
           // step 5 validate using TestNG Assertion
            Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to name sent in RequestBody" );
            Assert.assertEquals(res_job,req_job,"Job in ResposneBody is not equal to job sent in RequestBody");
            Assert.assertEquals(res_updatedAt,expecteddate,"createdAt is in ResponseBody is not equal to createdAt in request");
	           
           
	}

	
	

}
