package common_method;
import environment_repositaries.request_repo;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class api_trigger extends request_repo {
	static String headername = "Content-Type";
	 static String headervalue = "application/json";
	 
	 public static Response post_api_trigger(String requestBody,String endpoint) {
		 
		 RequestSpecification req_spec = RestAssured.given();
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response response = req_spec.post(endpoint);
			return response;
	}
	
public static Response put_api_trigger(String requestBody,String endpoint) {
		 
		 RequestSpecification req_spec = RestAssured.given();
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response response = req_spec.put(endpoint);
			return response;
	}
	
public static Response patch_api_trigger(String requestBody,String endpoint) {
	 
	 RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response response = req_spec.patch(endpoint);
		return response;
}

public static Response get_api_trigger(String requestBody,String endpoint) {
	 
	 RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		Response response = req_spec.get(endpoint);
		return response;
}

public static Response delete_api_trigger(String requestBody,String endpoint) {
	 
	 RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		Response response = req_spec.delete(endpoint);
		return response;
}


}

