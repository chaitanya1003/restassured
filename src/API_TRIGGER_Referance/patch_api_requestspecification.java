package API_TRIGGER_Referance;
import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class patch_api_requestspecification {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headervalue = "Content-Type";
		String headername = "application/json";
		String req_Body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";

		// step - 2 trigger the API
		// step - 3 build the req_specification to RequestSpecification
		RequestSpecification req_speci = RestAssured.given();

		// set the header
		req_speci.header(headername, headervalue);

		// set the reqBody
		req_speci.body(req_Body);

		// trigger the Api
		Response res = req_speci.post(hostname + resource);
		System.out.println(res);

		// extract the statuscode
		int code = res.statusCode();
		System.out.println(code);

		// fetch the res_Body parameter
		ResponseBody response_Body = res.getBody();
        System.out.println(response_Body.asString());
		
        String res_updatedAt = response_Body.jsonPath().getString("createdAt").toString().substring(0, 11);

		// step-4 generate expected date
		LocalDateTime currentDate = LocalDateTime.now();
		String exp_date = currentDate.toString().substring(0, 11);

		// step-5 validate using TestNG Assertion
        Assert.assertEquals(res_updatedAt, exp_date,
				"CreatedAT is in response body not equal to createdAt sent to req_body");


	}

}
